# TTS - Azul Summer Pavillion
is a LUA script written for [Tabletop Simulator](https://www.tabletopsimulator.com). Please note that this repository only contains the source code for the project and does not include the corresponding assets.

This mod was done by Steam user gilbertguillen and is included here only for reference.